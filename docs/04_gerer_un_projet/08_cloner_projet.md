---
author: Vous
title: Cloner un projet
tags:
  - Licence ouverte
  - Libre réutilisation de l'information
  - Attribution
---

Tout projet peut donner lieu à une bifurcation.

Vous avez remarqué un projet sur Porte-Plume et vous souhaitez le mettre en place dans votre classe ? Vous pouvez le cloner et le personnaliser à partir de sa présentation. Les champs seront pré-remplis, libre à vous de les modifier !
---
author: Vous
title: Supprimer un projet
tags:
  - Projet
---

1. Cliquer sur **Se connecter** puis sur le menu **Mes paramètres**
2. Atteindre la section **Demander la suppression de mon compte** en bas de page puis cliquer sur **Supprimer mon compte** puis sur **Supprimer**
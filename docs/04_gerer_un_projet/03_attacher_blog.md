---
author: Vous
title: Créer un blog
tags:
  - Blog
  - Publication ouverte
---

Pour associer un **blog** à un projet : 

1. **Me connecter** sur Porte-plume. 
2. Cliquer sur le menu haut **Mes publications**, puis retrouver le projet en question.
   ![Visuel du service connecté > Mes publications](images/mes_publications.png){ width=60% style="display: block; margin: 0 auto" }
3. Cliquer sur le projet, puis dans le menu latéral gauche, cliquer sur **Blog**, **Créer un blog**.
   ![Visuel de l'espace projet > Créer un blog](images/creer_blog.png){ width=60% style="display: block; margin: 0 auto" }
4. Si l'occasion s'y prête, prendre le temps de **personnaliser** le blog. C'est optionnel et peut être fait dans un second temps. 
   ![Visuel du formulaire Créer un blog](images/formulaire_creer_blog.png){ width=60% style="display: block; margin: 0 auto" }
   1. **Présenter le blog** en 250 caractères maximum
   2. **Ajouter des catégories** : elles constitueront les différents sous-menus de votre blog et seront visible dès lors qu'elles comporteront un article a minima.
   3. **Ajouter des modérateurs**, parmi les membres de votre équipe.
   4. **Choisir un groupe de couleur**, compatible avec le DSFR.
   5. **Choisir l'ordre de présentation** des articles. 
   6. Cliquer sur **Créer le blog**
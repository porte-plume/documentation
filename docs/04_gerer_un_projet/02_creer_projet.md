---
author: Vous
title: Créer un projet
tags:
  - Blog

---

Créer un projet est une fonctionnalité réservée aux **Personnels de l'Académie de Rennes**, authentifiés en cette qualité.

1. Cliquer sur **Me connecter** puis s'authentifier avec le service Toutatice, en tant que personnel de l'académie de Rennes.
![Visuel du Service Découverte avec la coche Personnel de l'académie de Rennes activée](images/se_connecter_entantque_personnel.png){ width=60% style="display: block; margin: 0 auto" }

2. Redirigé sur le fil d'actualité, l'usager clique sur le bouton d'action en haut de page **Créer un projet**. Le créateur d'un projet est **pilote** et **propriétaire**.

3. La création se déroule en trois étapes. 
    * Première étape : l'initialisation du projet
        ![Visuel de l'étape d'initialisation du projet](images/initialisation_projet.png){ width=60% style="display: block; margin: 0 auto" }
        * Indiquer **un titre**. Il pourra être modifié par la suite sur l'onglet **Paramètres du projet**. 
        !!! INFO info
            Le titre est, à ce stade, le seul champ obligatoire pour enregistrer le projet.
            Pour enregistrer le projet, cliquer deux fois sur **Suivant** puis **Créer le projet**
            **Tous les champs peuvent être modifiés a posteriori.** 
        * Le projet est rattaché à **l'établissement de rattachement** de celle ou celui qui le crée. Si ce personnel exerce dans plusieurs établissements, il sélectionne l'affiliation correcte. 
        * Optionnel. Associer un à plusieurs partenaires 
        * Cliquer sur **Suivant** pour passer à la deuxième étape.

    * Deuxième étape : définir la visibilité du projet
         ![Visuel de l'étape de visibilité du projet](images/visibilite_projet.png){ width=60% style="display: block; margin: 0 auto" }
        * Choisir le **niveau de partage** du projet : les seuls membres de l'équipe projet, les usagers d'un établissement ou bien le grand public. Ce partage peut évoluer au fil du projet. Au niveau établissement et académique, il est soumis à la modération des comités éditoriaux ad hoc. 
        * **Ajouter d'autres pilotes** : Le rôle du pilote consiste à initier des fonctionnalités (le blog, le salon de discussion), paramétrer la visibilité du projet, modérer les commentaires des articles de blog, gérer les rôles. Vous pouvez modifier votre équipe à tout moment dans l’onglet gérer les accès.
        ![Visuel erreur à l'invitation d'un pilote](images/erreur_invitation_pilote.png){ width=60% style="display: block; margin: 0 auto" }
        !!! ATTENTION danger
            Porte-Plume ne sait ajouter qu'un usager qui a déjà créé son compte. 
            Si le message suivant apparaît, merci de commencer par inviter la personne à rejoindre Porte-Plume pour ensuite ajouter ce pilote au projet
        * Cliquer sur **Suivant** pour passer à la deuxième étape.

    * Troisième étape : compléter la présentation
         ![Visuel de l'étape de visibilité du projet](images/completer_presentation.png){ width=60% style="display: block; margin: 0 auto" }
         * Ce formulaire permet d'indexer votre projet sur trois champs : 
            * Le **niveau d'enseignement** : cliquer sur la liste déroulante pour sélectionner un ou plusieurs cycles, voies concernées. 
            * Les **disciplines d'enseignement général** mobilisées sur le projet
            * Les **enjeux transversaux** 
        * Sélectionner la durée souhaitée : pour des raisons légales (effacement des données personnelles), elle ne peut excéder 12 mois.  
        * Compléter **le constat** à l'initiative du projet : quel diagnostic vient étayer l'aspect innovant de ce projet ?
        * Préciser **les objectifs** fixés au projet.
        * Énoncer **les compétences travaillées** au fil de l'eau. 
        * Il est possible d'ajouter une piste audio pour présenter le projet, soit par ajout d'un fichier, soit par enregistrement vocal.
        * Cliquer sur **Créer le projet**. La page se recharge : le projet est créé. Bienvenue sur le tableau de bord.

---
author: Vous
title: Clôturer un projet
tags:
  - Bilan
  - Pédagogie de projet
---

Tout projet a un commencement et une fin. 

Sur Porte-Plume la durée maximale d'un projet est de 12 mois. Après quoi il doit être clôturé. 

Tableau de bord du projet

Paramètres : Votre projet se termine ? Cliquer sur **Clôturer le projet**
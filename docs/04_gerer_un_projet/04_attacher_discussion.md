---
author: Vous
title: Créer une discussion
tags:
  - Messages instantanés
---

Pour associer une **discussion** à un projet : 

1. **Me connecter** sur Porte-plume. 
2. Cliquer sur le menu haut **Mes publications**, puis retrouver le projet en question.
   ![Visuel du service connecté > Mes publications](images/mes_publications.png){ width=60% style="display: block; margin: 0 auto" }
3. Cliquer sur le projet, puis dans le menu latéral gauche, cliquer sur **Discussion**, **Créer une discussion**.
   ![Visuel du service connecté > Créer une discussion](images/creer_discussion.png){ width=60% style="display: block; margin: 0 auto" }
4. Une fois la discussion créée, entamez la discussion avec l'ensemble des membres du cercle projet. 
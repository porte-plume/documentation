---
author: Vous
title: Qu'est-ce qu'un projet ?
tags:
    - Projet
---

Un projet désigne un collectif animé par une vision. Il s'outille en vue de réaliser un plan de travail pour atteindre des objectifs spécifiques et mesurer les progrès accomplis sur un temps donné.

Porte-Plume vise à outiller ces collectifs en proposant :
    * un **tableau de bord**, pour évaluer la mise en oeuvre du projet
    * un **outil d'écriture collaborative**, pour répondre aux objectifs de production collaborative
    * un **salon de discussion**, pour outiller l'ajustement des attentes de chacun au sein du collectif
    * un **outil de gestion d'équipe** et notamment de gestion des droits et autorisations de chacun.


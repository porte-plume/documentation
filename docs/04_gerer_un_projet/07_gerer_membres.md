---
author: Vous
title: Gérer les participants
tags:
  - Statuts et rôles
---

Un projet est à l'initiative d'un pilote, propriétaire du projet. 

Il peut inviter : 
* d'autres pilotes
* d'autres intervenants. 

Cela se passe au moment de la création du projet ou bien, après sa création, dans le menu latéral gauche **Gérer les accès**.

## Inviter des participants
Lorsqu'ils acceptent l'invitation, ces nouveaux participants sont simplement lecteurs. 


## Modifier le rôle des participants
Il appartient au pilote du projet de les hisser au rôle de contributeur. 

## Gérer les droits des participants mineurs 
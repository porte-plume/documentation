---
author: Vous
title: Modifier l'audience 
tags:
  - publication fermée / ouverte
  - processus de modération
  - droit des données personnelles
  - loi sur la liberté de la presse
---

Un projet peut être partagé à trois niveaux :
    - l'équipe-projet : les membres de l'équipe-projet peuvent être gérés dans le menu latéral gauche **Gérer les accès** du projet en question.
    - la communauté éducative : le projet sera accessible aux usagers de Porte-Plume faisant partie de la même école ou du même établissement que le pilote du projet.
    - l'académie et le grand public : tout usager, connecté ou non, à Porte-Plume pourra consulter le projet. Seuls les usagers connectés pourront interagir avec le projet (commenter, mettre en favoris, etc.)

    
---
author: Vous
title: Les cercles de publication
tags:
  - Explorer
  - Contenus indexés
---

Un cercle désigne une communauté éducative (une école, un établissement, une académie) et l'ensemble des contributions qui en relèvent. 

---
author: Vous
title: Modérer un commentaire
tags:
  - Article
  - Post
---

Un commentaire est créé à la suite d'un post ou d'un billet. Il est modéré de deux différentes façons.


## Les commentaires d'une présentation de projet
Modération a priori des pilotes du projet

## Les commentaires d'un post sur une discussion projet
Modération a posteriori => signaler un abus

## Personnels uniquement : les commentaires du fil d'actualité


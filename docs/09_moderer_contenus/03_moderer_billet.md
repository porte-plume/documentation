---
author: Vous
title: Modérer un billet
tags:
  - Blog
  - Article
---

Un billet est modéré à sa publication, à trois différents niveaux : 
- l'équipe projet
- la communauté éducative
- la Terre entière

A chaque niveau de modération est associé un comité éditorial, agissant sous la responsabilité : 
- du ou des pilotes au niveau du projet
- de la direction d'école ou d'établissement
- du Recteur de la région académique. 

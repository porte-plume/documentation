---
author: Hugues Labarthe
title: 🏡 Accueil
---


[**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} offre à la communauté éducative bretonne un espace pour **partager les belles expériences** vécues à l'École, en publiant librement : 

- **des posts**, messages courts et instantanés pour partager des activités au sein d'un collectif,
- **des billets**, articles de blog associant écriture, images, sons et code pour soutenir une citoyenneté numérique éclairée. 

Personnels, élèves et intervenants en milieu scolaire peuvent librement utiliser [**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} pour outiller une situation pédagogique ou éducative. 

[**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} présente plusieurs avantages : 

- **un accès facilité à tous les usagers**, grâce aux différents services d'authentification disponibles,
- **des processus intégrés** pour la gestion des autorisations parentales et la modération des contenus,
- **une suite collaborative** pour constituer son équipe, échanger, superviser et valoriser les activités menées. 

Avec [**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} tous les contenus sont autorisés, authentifiés et modérés. 

[**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} est un service incubé en [**Territoire Numérique Éducatif Finistère**](https://www.ac-rennes.fr/territoire-numerique-educatif-du-finistere-123169){:target="_blank"}. 
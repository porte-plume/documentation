---
author: Hugues Labarthe
title: En dernier recours, le lien magique
tags:
  - Guichet d'authentification
---

Sans authentification Toutatice ou France Connect, un usager peut cependant se connecter à Porte-Plume. Pour ce faire, il doit solliciter un garant qui lui créera sur demande un compte local. 

---
author: Hugues Labarthe
title: Configurer mon compte
tags:
  - Guichet d'authentification
  - France Connect
---


A la première connexion sur Porte-Plume, l'usager prend connaissance des données personnelles connues par le service et configure son compte. 

Les notifications (mail et mobile) sont désactivées par défaut. 

Si l'usager est personnel de l'Education nationale, préciser ses centres d'intérêt lui permet de recevoir des recommandations, sur la page d'accueil, sous forme de liens cliquables. 

La Charte éditoriale rappelle les fondamentaux du service : 
- favoriser une pédagogie de projets et une citoyenneté numérique éclairée, 
- soutenir la subjectivité et la pluralité des points de vue, 
- s'acculturer aux règles de la publication ouverte et responsable.

![Interface d'accession au service](images/acceder_service.png){ width=90% style="display: block; margin: 0 auto" }

En cliquant sur **Terminer**, l'usager a préalablement accepté la Charte éditoriale et les Conditions générales d'utilisation du service

---
author: Hugues Labarthe
title: Privilégier Toutatice
tags:
  - Guichet d'authentification
  - Toutatice
  - Educonnect
---

Toutatice est le portail web de l'éducation en Bretagne. 

1. Accéder à Porte-Plume en cliquant sur **Me connecter**, puis **S'identifier avec Toutatice**.
![Encart Se connecter à Toutatice](images/se_connecter_Toutatice1.png)


2. Le Service Découverte de Toutatice s'affiche. Cliquer sur le guichet d'authentification habituellement utilisé :  

- Élèves et responsables via **Educonnect**
- Personnels d'enseignement, d'éducation et d'encadrement via l'**annuaire académique** 
- Partenaires de la Région académique Bretagne via un **compte invité**
![Interface du Service Découverte de Toutatice](images/se_connecter_Toutatice2.png)


A l'issue de l'authentification, l'usager est redirigé vers le service Porte-Plume.

![Interface du Service Découverte de Toutatice](images/ecran_connecte.png){
En haut de page, à droite, le lien **Me connecter** affiche désormais l'identité de l'usager connecté.

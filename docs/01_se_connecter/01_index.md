---
author: Hugues Labarthe
title: Pourquoi se connecter ? 

tags:
  - Toutatice
  - Educonnect
  - France Connect
  - Lien magique
  - guichet d'authentification
---

[**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} a pour vocation de présenter **en libre-accès** les belles expériences vécues à l'École.
Ce service s'adresse à tous les publics, usagers, professionnels et curieux, venus de Bretagne et d'ailleurs.

La **création d'un compte** offre des fonctionnalités avancées de **personnalisation** :

- s'abonner à un projet, 
- recevoir des notifications instantanées,
- retrouver ses favoris sur une même page,
- compléter des autorisations parentales en ligne,
- commenter un contenu,
- etc.

!!! warning "ATTENTION"
    [**Porte-Plume.app**](https://porte-plume.app){:target="_blank"} ne sait pas faire le rapprochement pour une même personne entre ses deux comptes Toutatice et France Connect. 
    Il est donc essentiel de bien choisir son guichet d'authentification une fois pour toute.
    Un conseil : privilégiez votre compte Toutatice si vous avez la chance d'en avoir un !

## Les traitements de données à caractère personnel

**Porte-Plume traite des données à caractère personnel pour fonctionner en tant que service** : création de compte, activités en ligne, etc. 

- Porte-Plume s’inscrit dans le champ du service public du numérique éducatif défini à l’article L. 131-2 du *Code de l’éducation*. 
Le responsable de traitement n’est, par conséquent, pas tenu de recueillir le consentement des parents, des élèves ou des personnels sous sa responsabilité pour mettre en œuvre un tel traitement. 
- En validant les Conditions générales d'utilisation, les autres usagers authentifiés ont accepté le principe de ces traitements, déclarés au registre de l'académie. Ils peuvent à tout moment supprimer leur compte et les données associées.

**Porte-Plume traite également des données à caractère personnel en tant que média** : des éléments textuels, visuels ou vocaux permettant d'identifier une personne. 
Ces données à caractère personnel ne peuvent être publiées sans le consentement de la personne visée et, dans le cas d'un élève mineur, de l'un de ses responsables légaux. 

Porte-Plume offre deux services pour simplifier cette mise en oeuvre : 

- **un module de supervision** des autorisations d'enregistrement et de diffusion de données à caractère personnel. C'est un outil de dialogue avec les responsables légaux pour défendre le respect des droits de chacun et l'intérêt de l'élève. 
  ![Encart Données à caractère personnel](images/gerer_autorisations_DCP.png){ width=60% style="display: block; margin: 0 auto; border: 1px solid #ddd; border-radius: 4px; padding: 5px;"}

- **un formulaire pédagogique** '*Pouvez-vous publier ce billet ?*' adressé aux élèves sollicitant la publication d'un billet de blog.
  ![Encart Données à caractère personnel](images/form_DCP.png){ width=60% style="display: block; margin: 0 auto; border: 1px solid #ddd; border-radius: 4px; padding: 5px;"}

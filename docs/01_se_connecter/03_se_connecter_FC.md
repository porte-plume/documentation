---
author: Hugues Labarthe
title: Opter pour France Connect
tags:
  - Guichet d'authentification
  - France Connect
---

France Connec est une alternative pour tous les usagers qui ne bénéficient pas d'un compte Toutatice. 

France Connect est une solution proposée par l’État pour sécuriser et simplifier la connexion à plus de 1400 services en ligne. 

1. Cliquer sur **Je n'ai pas d'identifiant Toutatice** puis cliquez sur **S'identifier avec France Connect**.
![Encart Se connecter via France Connect](images/se_connecter_FC.png){ width=60% style="display: block; margin: 0 auto" }

2. Choisir un compte, entrer ses identifiants et mots de passe. 

3. Cliquer sur **Continuez sur Porte-Plume**
![Page après authentification sur France Connect](images/se_connecter_FC2.png){ width=60% style="display: block; margin: 0 auto" }


A l'issue de l'authentification, l'usager est redirigé vers le service Porte-Plume.

![Interface du Service Découverte de Toutatice](images/ecran_connecte.png){ width=90% style="display: block; margin: 0 auto" }
En haut de page, à droite, le lien **Me connecter** affiche désormais l'identité de l'usager connecté.
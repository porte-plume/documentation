---
author: Hugues Labarthe
title: Choisir un nom d'auteur
tags:
  - Guichet d'authentification
  - France Connect
---

Seuls les élèves sont concernés par cette étape. 

Porte-Plume ne révèle pas l'identité des élèves au grand public. 
Toute contribution est ainsi publiée sous nom d'auteur. 

Seuls les personnels Éducation nationale ont vocation à connaitre l'identité de ce contributeur. 

Le nom d'auteur est donc un nom d'emprunt :
- Il ne doit comporter aucune allusion à l'identité. 
- Il permet de cautionner des écrits, de les rassembler sous un nom de plume. 


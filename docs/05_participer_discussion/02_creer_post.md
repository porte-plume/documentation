---
author: Vous
title: Créer un post simple
tags:
  - Discussion
---

Un post comprend a minima : 
* des destinataires,
* un texte allant jusqu'à 500 caractères.


1. Cliquer sur **Me connecter** puis s'authentifier avec le service de son choix.

2. En fonction de son statut ou son rôle, l'usager accède à différentes fonctionnalités : 
  * Avec le statut **Personnel de l'Académie de Rennes**, l'accueil est un fil d'actualité, une conversation sur les actions éducatives en cours dans les écoles et établissements de la région académique Bretagne.  
  ![Visuel de l'accueil Fil d'actualité](images/btn_creer_post.png){ width=60% style="display: block; margin: 0 auto" }

  * Si votre rôle dans votre projet est celui de **contributeur** ou **pilote**, alors, si cette fonctionnalité a bien été activée par le pilote, vous retrouverez dans l'espace de ce projet, un onglet Discussion. 
   ![Visuel de la discussion sur un projet](images/btn_creer_post_discussion.png){ width=60% style="display: block; margin: 0 auto" }

3. Un clic sur le bouton d'action **Créer un post** permet d'accéder au formulaire d'envoi.
   ![Visuel de la discussion sur un projet](images/form_creer_post.png){ width=30% style="display: block; margin: 0 auto" }
  1. L'usager choisit son cercle de publication : 
     * sur le fil d'actualité, les personnels de son établissement ou de la région académique,
     * sur un projet, les pilotes, les pilotes et contributeurs ou l'ensemble des membres (pilotes, contributeurs et lecteurs).
  2. Il contribue par un texte de 500 caractères maximum.

4. Au clic sur Publier, l'écran est actualisé avec la dernière contribution.
---
author: Vous
title: Joindre un sondage
tags:
  - Discussion
  - Sondage
---
Un sondage désigne une question à choix multiple et réponse unique.

!!! INFO info
    Lorsque l'usager a répondu au sondage, l'affichage des résultats est actualisé. 
    ![Visuel Création d'un post sondage](images/publi_post_sondage.png){ width=30% style="display: block; margin: 0 auto" }    

1. Commencer par créer un post simple puis cliquer sur l'icône **Créer un sondage**, la troisième sous le champ texte, en surbrillance dans l'image suivante.
  ![Visuel Création d'un post sondage](images/btn_creer_post_sondage.png){ width=60% style="display: block; margin: 0 auto" }

2. Un sondage peut comporter jusqu'à 4 options. La question doit figurer dans le champ texte qui précède. 

3. Pour supprimer le sondage, cliquer sur le bouton **Supprimer le média**.


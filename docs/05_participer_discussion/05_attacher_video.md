---
author: Vous
title: Joindre une vidéo
tags:
  - Discussion
---
---
Une vidéo a pour taille maximale 500 Mo. 
Elle répond à l'un des formats supportés : .mp4.

1. Commencer par créer un post simple puis cliquer sur l'icône **Ajouter une vidéo**, deuxième icone à gauche sous le champ texte principal/
  ![Encart Se connecter à Toutatice](images/form_ajouter_video.png){ width=30% style="display: block; margin: 0 auto; border: 1px solid #ddd; border-radius: 4px; padding: 5px;"}

2. Ajouter la vidéo depuis son poste. 

3. Pour supprimer la vidéo, cliquer sur le bouton **Supprimer le média**.
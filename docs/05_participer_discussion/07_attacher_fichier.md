---
author: Vous
title: Joindre un fichier
tags:
  - Discussion
---

Un fichier bureautique désigne les extensions usuelles :

- traitement de texte : .odt
- feilles de calcul : ods
- présentation : .odp

!!! ATTENTION danger
    L'usage des extensions propriétaires suivantes est déconseillé : .doc, .docx, .xls, .xlsx, .ppt, .pptx
    Le format .pdf est désormais un standard international mais ce type de fichier ne permet pas une réappropriation aisée. 
    Il est vivement conseillé d'aller sur d'autres formats ouverts. 
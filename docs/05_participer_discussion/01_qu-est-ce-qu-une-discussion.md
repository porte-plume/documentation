---
author: Vous
title: Qu'est-ce qu'une discussion ?
tags:
  - Discussion
  - Micro-blogging
---

Porte-plume permet d'échanger des **posts**, des messages courts et instantanés, sur deux types de discussions : 
* Un **fil de discussion** initié au sein d'un projet spécifique, accessible à ses seuls membres. **Pilotes et contributeurs** créent des posts et les commentent. Les **lecteurs** en sont seulement notifiés. 
* Un **fil d'actualité** réservé aux **Personnels de l'Académie de Rennes**.

Un **post** a une longueur maximale de 500 caractères et peut se présenter sous la forme d'un texte, une image ou une vidéo légendées, un sondage, un vocal ou un fichier.

Il peut être commenté, partagé, liké et signalé.


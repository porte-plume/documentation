---
author: Vous
title: Joindre une image
tags:
  - Discussion
---
---
Une image a pour taille maximale 50 Mo. 
Elle répond à l'un des formats supportés : .png, .jpg, .jpeg.

1. Commencer par créer un post simple puis cliquer sur l'icône **Ajouter une image**, première icone à gauche sous le champ texte principal/
  ![Visuel Création d'un post avec image](images/form_ajouter_image.png){ width=60% style="display: block; margin: 0 auto" }

2. Ajouter l'image depuis son poste. 

3. Pour supprimer l'image, cliquer sur le bouton **Supprimer le média**.


---
author: Vous
title: Contribuer à documenter Porte-Plume
tags:
  - Mode d'emploi
  - Forge
---

Cette documentation est contribuable sur la **[Forge des Communs Numériques Éducatifs](https://docs.forge.apps.education.fr){:target="_blank"}**.

Une forge fonctionne sur un système de **branches**. 

- La documentation constitue la **branche principale** du projet.  
- Lorsque des modifications sont effectuées, il faut les **pousser** ce qui **crée une nouvelle branche**.  
- Il faut alors demander à **fusionner** cette nouvelle branche à la branche principale pour **appliquer les modifications**.

## Contribuer en Markdown

Les pages sont rédigées en **Markdown**, le langage de balisage le plus simple au monde. 
Voici les éléments de syntaxe utilisés.

``` title="Syntaxe de base pour la mise en forme en Markdown"
• Insérer des titres
  ## Titre de niveau 2
  ### Titre de niveau 3

• Insérer un lien simple
  [nom du lien](url du lien)
• Insérer un lien qui s'ouvre dans un nouvel onglet
  [nom du lien](url du lien){:target="_blank"}

• Insérer une image
  ![Texte alternatif pour l'accessibilité](images/nom_du_fichier.png)
• Insérer une image avec indication de largeur de page
  ![Texte alternatif pour l'accessibilité](images/nom_du_fichier.png){ width=60%}

• Mettre en forme
  ** texte en gras **
  * texte en italique *
  ~~ texte barré~~
  *** texte est en gras et italique***

• Citer
  > C'est une citation

• Liste : utiliser indifféremment - * + mais surtout faire précéder d'un saut de ligne

  - George Washington
  * John Adams
  + Thomas Jefferson
```

## Rejoindre la documentation sur GitLab
1. **Aller sur la [page dédiée au projet](https://forge.apps.education.fr/porte-plume/documentation){:target="_blank"}**. Cliquer sur le guichet d'authentification **apps.education.fr** ![Encart Se connecter à apps.education.fr](images/forge_authentification_apps.png){ width=90%}

2. S'identifier avec **l'Authentification Education Nationale** (même identification que le webmail). Après redirection sur la [page du projet](https://forge.apps.education.fr/porte-plume/documentation){:target="_blank"}, l'initiale cerclée de l'usager apparaît sur la barre latérale gauche : il est bien connecté. ![Encart Repérer l'initiale cerclée de son prénom](images/forge_barre_laterale.png){ width=60%} Si la barre latérale de navigation est masquée, survoler l'icône **Garder la barre latérale visible**. ![Encart Ouvrir la barre latérale de navigation](images/forge_barre_nonvisible.png){ width=60%}
 

## Modifier le contenu d'une page

### Via la page web de la Forge
La page de la forge permet aussi de modifier les contenus, fichier par fichier.

1. Sur la page d'accueil du projet de la documentation, cliquer sur **docs**.

2. Ouvrir le **répertoire** souhaité puis sur la **page** à laquelle vous souhaitez contribuer.

3. Cliquer sur **Modifier** puis sur **Modifier le fichier unique**.  
Une page s'ouvrira, sur laquelle vous pourrez modifier le fichier en suivant **la syntaxe Markdown**. Cliquer sur **Aperçu** pour voir un **aperçu en temps réel** de la page en même temps que vous éditez le fichier.

4. Une fois les modifications réalisées, s'assurer que la case **Créer une demande de nouvelle requête de fusion avec ces changements** est cochée. Cliquer ensuite sur **Valider les modifications** pour lancer une nouvelle demande de fusion, comme [expliqué précédemment](#appliquer-les-modifications).


### Via l'interface web IDE
Il est possible de modifier le contenu de la documentation via une **interface web**.

1. En haut de page, cliquer sur **Modifier** puis sur **Web IDE**. 
![Encart Accéder à l'éditeur Web](images/forge_modifier_WebIDE.png){ width=90%} 

2. Sur la barre latérale de navigation, ouvrir le répertoire souhaité, puis cliquer le titre de la page à contribuer : elle s'affiche en pleine page.
![Encart Accéder à la page à modifier](images/forge_acceder_page.png){ width=60% } 

3. En haut de page, cliquer sur **Modifier** puis sur **Web IDE**. 
![Encart Accéder à l'éditeur Web](images/forge_modifier_WebIDE.png){ width=90% style="display: block; margin: 0 auto; border: 1px solid #ddd; border-radius: 4px; padding: 5px;"}. 

4. Sur la barre latérale de navigation, ouvrir le répertoire souhaité, puis cliquer le titre de la page à contribuer : elle s'affiche en pleine page.
![Encart Accéder à la page à modifier](images/forge_acceder_page.png){ width=60% style="display: block; margin: 0 auto; border: 1px solid #ddd; border-radius: 4px; padding: 5px;"}. 

5. Apporter les modifications souhaitées en suivant la **syntaxe Markdown**

6. Pour **pousser** et **fusionner** les modifications sur la branche principale  :

  1. Cliquer sur l'icone **Source control**.
  2. Dans l'encart **Commit message**, renseigner les modifications réalisées, puis cliquer sur **Commit and push to 'main'** puis sur **Create new branch**.
  3. Une boîte de dialogue s'ouvrira. Appuyer sur la touche **Entrer** du clavier *sans écrire d'autre*.  
  Vous venez de **pousser une modification**, mais il faut réaliser une **demande de fusion** pour que le changement puisse être vérifié et accepté.
  4. Sur la même page, cliquer sur **Create MR**, en bas à droite de l'écran. Une nouvelle page doit s'ouvrir.
  5. Dans l'encart **Description**, décrire les modifications réalisées si nécessaire.
  6. Dans l'onglet **Personne assignée**, choisissez **Hugues Labarthe** pour relire et valider les modifications. Il n'est pas nécessaire de renseigner les autres onglets.
  7. S'assurer que la case **Supprimer la branche source lorsque la requête de fusion est acceptée**, puis cliquer sur **Créer la requête de fusion**.  
  Votre requête de fusion est envoyée, si elle est acceptée, vos modifications seront visibles sur la page de la documentation.
![Encart Faire une requête de fusion](images/forge_commit_request.png){ width=60% }

## Pour aller plus loin...

Tutoriel suivi pour créer ce site : [NSI Normandie : Atelier forge.apps.education.fr](https://nsinormandie.forge.apps.education.fr/2024-intro-forge/02_creation_site_mkdocs/){:target="_blank"}

Merci à Jean-Matthieu Barbier, Nathalie Maïer et Nathalie Weibel pour ce partage.

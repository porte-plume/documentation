---
author: Vous
title: Compléter les autorisations parentales 
tags:
  - Règlement Général de Protection des Données
  - données personnelles
---

La publication d'une contribution d'une personne mineure vers un plus large public exige de préalablement recueillir **l'autorisation parentale d'enregistrement et d'utilisation de l'image/la voix d'une personne mineure**. 

Comment procéder ?

L'un des responsables légaux (parents ou tuteur) se connecte sur Porte-Plume. 
Sur l'écran d'accueil, les différents projets auquel un enfant est associé sont listés. 
![Capture d'écran : Accueil du responsable légal : compléter les autorisations](images/RL_completer_autorisations.png){ width=80% style="display: block; margin: 0 auto" }

Cliquer sur **Compléter** pour accéder au formulaire d'autorisation. 
![Capture d'écran : Formulaire d'autorisation des données personnelles](images/formulaire_autorisations.png){ width=60% style="display: block; margin: 0 auto" }

Après avoir pris connaissance des spécificités du projet éducatif proposé, le responsable légal est invité à compléter trois questions relatives à 

- l'enregistrement et la diffusion de données personnelles sur son enfant
- la diffusion d'une contribution sous nom d'auteur en licence ouverte.

Par contribution, on entend une quelconque création originale : article, dessin, podcast, montage vidéo. 

- Une contribution peut comporter des données personnelles (le nom d'un interviewé), mais des dispositions peuvent être prises pour préserver la vie privée de chacun. 
- Cette contribution est publiée sous [nom d'auteur](https://porte-plume.forge.apps.education.fr/documentation/se_connecter/nom_d_auteur/){ .md-button target="_blank" rel="noopener" } en [Licence ouverte 2.0](https://github.com/etalab/licence-ouverte/blob/master/LO.md){ .md-button target="_blank" rel="noopener" } (réutilisation libre de l'information).  

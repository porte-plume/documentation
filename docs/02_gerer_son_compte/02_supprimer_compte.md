---
author: Hugues Labarthe
title: Supprimer son compte

tags:
  - Règlement Général de Protection des Données
  - données personnelles
---

En application de l'article 17.1 du Règlement général sur la protection des données (RGPD), l'usager peut, à tout moment demander la suppression de l’ensemble de ses données personnelles.

Pour supprimer son compte, cliquer sur **Me connecter** puis **Mes Paramètres** et descendre vers la section **Demander la suppression de mon compte**. 

![Encart Supprimer son compte](images/supprimer_son_compte.png){ width=60% style="display: block; margin: 0 auto" }

Cliquer sur **Supprimer mon compte**.
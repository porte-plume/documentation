---
author: Hugues Labarthe
title: Préciser ses centres d'intérêt

---

Si vous êtes un personnel de l'Éducation nationale, Porte-Plume vous propose de préciser vos centres d'intérêt, pour améliorer les recommandations qui vous sont destinées.

1. Cliquer sur **Me connecter** puis sur **Mes paramètres**
2. Dans la section **Autres actions**, cliquer sur **Gérer mes centres d'intérêts**
3. Entrer un mot clé en lien avec ses centres d'intérêt. S'il apparaît dans la liste, le cocher puis cliquer sur **Ajouter**
4. Si le mot-clé recherché ne figure pas dans la liste pré-établie, cliquer sur **Proposer un nouveau centre d'intérêt**
5. Pour un nouveau mot-clé, indiquer le nom du centre d'intérêt puis cliquer sur **Envoyer**
6. La proposition d'ajout sera analysée pour une éventuelle actualisation du site. 

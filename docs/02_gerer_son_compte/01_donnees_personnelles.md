---
author: Hugues Labarthe
title: Les données personnelles

tags:
  - Règlement Général de Protection des Données
  - données personnelles
---

Conformément à l’article 30 du RGPD, le service Porte-Plume est déclaré au registre des traitements de la Région académique Bretagne. 

Porte-Plume traite des données personnelles sur la base légale de l'intérêt légitime, pour :

- identifier le détenteur du compte, son statut et ses rôles, 
- publier des contenus (messages courts, projets pédagogiques, billets de blogs), 
- gérer les autorisations de diffusion des données personnelles et oeuvres de l'esprit des élèves mineurs.

Pour consulter les données personnelles transmises par le guichet d'authentification à Porte-Plume, cliquer sur **Me connecter**, sur **Mes paramètres** puis descendre sur la section **Mes données personnelles**.
![Encart Données personnelles transmises par le guichet d'authentification](images/donnees_personnelles.png){ width=60% }

Pour demander la rectification de l'email, merci d'envoyer un message à porte-plume@ac-rennes.fr

Pour supprimer son compte et l'ensemble de ses données personnelles, consulter la rubrique suivante. 
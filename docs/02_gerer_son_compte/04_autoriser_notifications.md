---
author: Vous
title: Autoriser les notifications mail
---

A tout instant, la communauté Porte-Plume contribue, sollicite, publie des contenus susceptibles de rencontrer l'intérêt d'un usager. 

Les **notifications mails** sont là pour garder le fil, dans sa messagerie, avec ce qui se joue sur https://porte-plume.app.

1. Cliquer sur **Me connecter** puis **S'identifier** avec le guichet d'authentification de votre choix.
2. Cliquer sur **Mes paramètres** puis dans la section **Notifications**, cliquer sur **Recevoir les notifications par mail**. 
3. Votre mail est erroné ? Merci de rectifier l'adresse auprès de porte-plume@ac-rennes.fr avec pour objet du mail **Rectification email**

!!! ATTENTION danger
    Si vous souhaitez recevoir des notifications instantanées sur votre smartphone, ne pas cliquer sur **Recevoir les notifications web push** avec votre ordinateur.
    Installez l'application Porte-Plume puis, activez les notifications web push avec votre smartphone. 





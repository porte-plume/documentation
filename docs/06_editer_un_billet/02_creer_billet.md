---
author: Vous
title: Créer un billet
tags:
  - Blog
  - Article
---

Un billet est créé par un usager de Porte-Plume invité sur un projet avec un rôle de contributeur ou de pilote. 

## Créer et publier un billet

### Si vous êtes _contributeur_
1. Commencer par se connecter à Porte-Plume.
2. Cliquer sur l'onglet **Mes publications**.
3. Cliquer sur le **projet** pour lequel vous voulez publier un billet.
4. Sur le volet de gauche, cliquer sur **Blog** puis sur **Voir le blog**.
5. Cliquer ensuite sur **Créer un nouveau billet**.
6. Choisissez un ou plusieurs **référents** parmi les pilotes du projet.  
*Remarque : si vous êtes un contributeur **responsable légal**, vous n'avez pas besoin de référent*. 
7. Remplir le **titre** et le **contenu** du billet.
8. Pour **publier** le billet :  
   a. Cliquer sur **Solliciter un avis**.  
   b. Remplir le petit formulaire de respect du cadre juridique.  
   c. Cliquer sur **Envoyer**.  
   d. Votre billet est maintenant soumis à la **modération** des pilotes, vous pouvez le modifier en le passant en **brouillon**.

### Si vous êtes _pilote_
1. Commencer par se connecter à Porte-Plume.
2. Cliquer sur l'onglet **Mes publications**.
3. Cliquer sur le **projet** pour lequel vous voulez publier un billet.
4. Sur le volet de gauche, cliquer sur **Blog** puis sur **Voir le blog**.
5. Cliquer ensuite sur **Créer un nouveau billet**.
6. Remplir le **titre** et le **contenu** du billet.
7. Cliquer sur **Publier** pour publier le billet.

En tant que pilote, vous avez la possibilité de **modérer** les billets des contributeurs de votre projet. Pour en savoir plus, rendez-vous dans la section [Modérer un billet](https://porte-plume.forge.apps.education.fr/documentation/09_moderer_contenus/03_moderer_billet/).

Si vous êtes **pilote référent** d'un billet, vous pouvez **éditer ce billet**.

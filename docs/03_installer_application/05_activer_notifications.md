---
author: Vous
title: Activer les notifications web push
tags:
  - Progressive Web App
  - Android
  - iOS
---

Pour recevoir des notifications instantanées sur un terminal, il est impératif d'activer les notifications depuis l'écran de ce terminal.

1. Cliquer sur **Se connecter**
2. Cliquer sur le menu **Mes paramètres**
3. Cliquer sur **Recevoir les notifications web push**

![Encart Activer les notifications web push](images/activer_webpush.png){ width=30% style="display: block; margin: 0 auto" }
<!--- Quelle astuce pour vérifier si les notifs sont bien activées ? --->
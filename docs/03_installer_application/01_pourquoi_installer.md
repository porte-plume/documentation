---
author: Vous
title: Pourquoi et où installer l'application ?
tags:
  - Progressive Web App
  - Android
  - iOS
---

L'application Porte-Plume fonctionne sur toutes les plateformes : iOS, Android et web. 

Elle offre une expérience semblable à celle d’une application mobile native.
- notifications en temps réel
- ergonomie adaptée
- authentification persistante

!!! ATTENTION danger
    Les notifications ne peuvent être envoyées que vers un seul terminal : pc, tablette et/ou smartphone. 
    Choisir un terminal dont l'usage est personnel.
    Une fois l'installation effectuée, utiliser ce terminal pour cliquer sur **Recevoir les notifications web push**
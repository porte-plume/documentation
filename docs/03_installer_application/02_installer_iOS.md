---
author: Vous
title: Sur iOS
tags:
  - Web-app
  - iOS
---

Se munir de son terminal iOS. Depuis le navigateur Safari :

1. Cliquez sur l'icone hamburger en haut à droite de votre écran puis sur **Me connecter**. Connectez-vous avec le service de votre choix (Toutatice, France Connect, compte local)
![Encart Installer l'application sur iOS](images/installer_menu_iOS.jpg){ width=30% style="display: block; margin: 0 auto" }

2. Une fois connectés, cliquez de nouveau sur l'icone hamburger puis sur **Installez l'application**.
![Encart Installer l'application sur iOS](images/installer_btn_iOS.jpg){ width=30% style="display: block; margin: 0 auto" }

3. Un tutoriel s'affiche. Cliquez sur **l'icone Partage** de votre navigateur, puis choisissez l'option **Partager sur l'écran d'accueil**
![Encart Installer l'application sur iOS](images/installer_tuto_iOS.jpg){ width=30% style="display: block; margin: 0 auto" } ![Encart Installer l'application sur iOS](images/installer_ajouter_accueil_iOS.jpg){ width=30% style="display: block; margin: 0 auto" }

Vous retrouverez désormais l'application Porte-Plume sur votre écran d'accueil.
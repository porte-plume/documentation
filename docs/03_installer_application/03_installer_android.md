---
author: Vous
title: Sur Android
tags:
  - Web-app
  - Android
---

Se munir de son terminal Android : 

1. Cliquer sur **Me connecter** puis **S'identifier** avec le service souhaité (Toutatice, France Connect, compte local).

2. Sur la droite de l'écran d'accueil, si la version du navigateur est compatible, un bouton **Installer l'application** s'affiche. 
![Encart Installer l'application sur Android](images/installer_btn_android.png){ width=90% style="display: block; margin: 0 auto" }

3. Cliquer dessus : un tutoriel s'affiche.
![Encart Installer l'application sur Android](images/installer_android.png){ width=30% style="display: block; margin: 0 auto" }

4. Sur la barre de navigation, en haut d'écran, cliquer sur le menu avec trois points verticalement alignés.
![Encart Installer l'application sur Android](images/installer_kebab_android.png){ width=30% style="display: block; margin: 0 auto" }

5. Cliquer sur **Ajouter à l'écran d'accueil**
![Encart Ajouter à l'écran d'accueil sur Android](images/ajouter_accueil_android.png){ width=30% style="display: block; margin: 0 auto" }

A présent, Porte-Plume a rejoint les applications en écran d'accueil. Elle apparaît sous une icône de Marianne. 